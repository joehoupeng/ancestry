﻿namespace Data
{
    public class PersonViewModel : Person
    {
        public string BirthPlace { get; set; }
        public string Ancestors { get; set; }
        public string Descendants { get; set; }
    }
}
