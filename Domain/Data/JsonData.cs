﻿using System.Collections.Generic;

namespace Data
{
    public class JsonData
    {
        public IEnumerable<Place> places { get; set; }
        public IEnumerable<Person> people { get; set; }
    }
}
