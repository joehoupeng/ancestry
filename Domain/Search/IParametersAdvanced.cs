﻿using Library;

namespace Search
{
    public interface IParametersAdvanced : IParameters
    {
        AncestryOption AncestryOption { get; set; }
    }
}
