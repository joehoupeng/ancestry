﻿using Library;

namespace Search
{
    public interface IParameters
    {
        string Keyword { get; set; }
        Gender Gender { get; set; }
    }
}
