﻿namespace Library
{
    public enum AncestryOption
    {
        Ancestor,
        Descendant
    }
}
