﻿

using System;
using System.ComponentModel;
using System.Threading;

namespace Library
{
    public static class ObjectFactory
    {
        private static readonly Lazy<Container> _containerBuilder =
                new Lazy<Container>(GetDefaultContainer, LazyThreadSafetyMode.ExecutionAndPublication);

        public static IContainer Container
        {
            get { return _containerBuilder.Value; }
        }

        private static Container GetDefaultContainer()
        {
            return new Container();
        }
    }
}
