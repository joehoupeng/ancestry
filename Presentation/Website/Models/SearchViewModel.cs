﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Data;

namespace Website.Models
{
    public class SearchViewModel
    {
        [Required]
        public string Keyword { get; set; }
        [Display(Name="Male")]
        public bool IsMale { get; set; }
        [Display(Name = "Female")]
        public bool IsFemale { get; set; }

        public IEnumerable<PersonViewModel> SearchResult { get; set; }
        
        public bool IsDescendant { get; set; }
    }
}
