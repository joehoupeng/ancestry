﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Data;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Website.Models;

namespace Website.Controllers
{
    public partial class HomeController : BaseController
    {
        private string GetQueryString(string keyword = "", bool isMale = false, bool isFemale = false)
        {
            var queryString = $"name:{keyword}";
            if (isMale && !isFemale) queryString = queryString + " && gender:m";
            if (!isMale && isFemale) queryString = queryString + " && gender:f";
            return queryString;
        }
        private IEnumerable<PersonViewModel> LuceneSearch(string queryString, string fieldName = "name")
        {
            var list = new List<PersonViewModel>();
            var indexFile = ConfigurationManager.AppSettings["IndexFile"];
            var indexReader = IndexReader.Open(FSDirectory.Open(new System.IO.DirectoryInfo(indexFile)), true);
            var searcher = new IndexSearcher(indexReader);
            var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
            var field = fieldName;
            var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_30, field, analyzer);
            var query = parser.Parse(queryString);
            var result = searcher.Search(query, null, 100);
            if (result.TotalHits > 0)
            {
                list.AddRange(result.ScoreDocs.Select(scoreDoc => searcher.Doc(scoreDoc.Doc)).Select(document => new PersonViewModel()
                {
                    id = Convert.ToInt32(document.fields_ForNUnit[0].StringValue),
                    name = document.fields_ForNUnit[1].StringValue,
                    gender = document.fields_ForNUnit[2].StringValue,
                    BirthPlace = document.fields_ForNUnit[3].StringValue,
                    Ancestors = document.fields_ForNUnit[5].StringValue,
                    Descendants = document.fields_ForNUnit[6].StringValue
                }));
            }
            return list;
        }
        private IEnumerable<PersonViewModel> Search(string keyword, bool isMale, bool isFemale)
        {
            return !string.IsNullOrWhiteSpace(keyword) ? LuceneSearch(GetQueryString(keyword, isMale, isFemale)) : new List<PersonViewModel>();
        }

        public ActionResult Index(string keyword = "", bool isMale = false, bool isFemale = false)
        {
            var result = Search(keyword, isMale, isFemale);

            return View(new SearchViewModel
            {
                Keyword = keyword,
                IsMale = isMale,
                IsFemale = isFemale,
                SearchResult = result
            });
        }

        private string GetAdvancedQueryString(string keyword = "", bool isMale = false, bool isFemale = false, bool isDescendant = false)
        {
            var queryString = $"fullname:{keyword.ToLower().Replace(" ", "")}";
            if (isMale && !isFemale) queryString = queryString + " && gender:m";
            if (!isMale && isFemale) queryString = queryString + " && gender:f";
            return queryString;
        }
        private IEnumerable<PersonViewModel> AdvancedSearch(string keyword, bool isMale, bool isFemale, bool isDescendant)
        {
            return !string.IsNullOrWhiteSpace(keyword) ? LuceneSearch(GetAdvancedQueryString(keyword, isMale, isFemale, isDescendant), "fullname") : new List<PersonViewModel>();
        }
        public ActionResult Advanced(string keyword = "", bool isMale = false, bool isFemale = false, bool isDescendant = false)
        {
            var searchResult = AdvancedSearch(keyword, isMale, isFemale, isDescendant);
            if (searchResult.Any())
            {
                var firstMatch = searchResult.FirstOrDefault();
                var ancestryIDs = firstMatch.Ancestors;
                if (isDescendant)
                {
                    ancestryIDs = firstMatch.Descendants;
                }
                if (!string.IsNullOrWhiteSpace(ancestryIDs))
                {
                    var ancestryIDsList = ancestryIDs.Split(',');
                    var queryList = new List<string>();
                    foreach (var ancestry in ancestryIDsList)
                    {
                        queryList.Add("idstring:id" + ancestry);
                    }
                    var ancestryDocuments = LuceneSearch(string.Join(" or ", queryList), "idstring");


                    return View(new SearchViewModel
                    {
                        Keyword = keyword,
                        IsMale = isMale,
                        IsFemale = isFemale,
                        SearchResult = ancestryDocuments
                    });
                }

            }


            return View(new SearchViewModel
            {
                Keyword = keyword,
                IsMale = isMale,
                IsFemale = isFemale,
                SearchResult = Enumerable.Empty<PersonViewModel>()
            });
        }

    }
}