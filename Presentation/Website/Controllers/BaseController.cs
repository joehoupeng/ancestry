﻿using System.Configuration;
using System.Web.Mvc;
using AppServices;
using StructureMap;
using Lucene.Net;

namespace Website.Controllers
{
    internal class StructureMapBootStrapper
    {
        internal static IContainer Container
        {
            get
            {
                var container = new Container(x =>
                {
                    
                });
                return container;
            }
        }
    }
    public class BaseController : Controller
    {
        public IContainer Container => StructureMapBootStrapper.Container;
    }
}