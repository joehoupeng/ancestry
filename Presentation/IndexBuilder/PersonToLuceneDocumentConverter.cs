﻿using System.Collections.Generic;
using Data;
using Lucene.Net.Documents;

namespace IndexBuilder
{
    public static class PersonToLuceneDocumentConverter
    {
        public static Document Convert(PersonViewModel person, string ancestors, string descendants)
        {
            var document = new Document();

            var fieldId = new Field("id", person.id.ToString(), Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldId);
            var fieldName = new Field("name", person.name, Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldName);
            var fieldGender = new Field("gender", person.gender, Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldGender);
            var fieldPlace = new Field("place", person.BirthPlace, Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldPlace);
            var fieldFullName = new Field("fullname", person.name.ToLower().Replace(" ",""), Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldFullName);
            var fieldAncestors = new Field("ancestors", ancestors, Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldAncestors);
            var fieldDescendants = new Field("descendants", descendants, Field.Store.YES, Field.Index.ANALYZED);
            document.Add(fieldDescendants);
            var fieldIdString = new Field("idstring", "id"+person.id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED);
            document.Add(fieldIdString);

            return document;
        }
    }
}
