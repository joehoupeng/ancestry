﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Data;
using Library;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using AppServices.JsonFile;
using StructureMap;
using Directory = System.IO.Directory;

namespace IndexBuilder
{

    internal class StructureMapBootStrapper
    {
        internal static IContainer Container
        {
            get
            {
                var appSettings = ConfigurationManager.AppSettings;
                var container = new Container(x =>
                {
                    x.For<ILoader>().Use<Loader>()
                                        .Ctor<string>("fileName").Is(appSettings["JsonFileName"]);
                });
                return container;
            }
        }
    }
    class Program
    {
        private static JsonData GetJsonDataObject()
        {
            return StructureMapBootStrapper.Container.GetInstance<ILoader>().Load();
        }
        private static IEnumerable<Document> GetDocuments(JsonData data)
        {
            var viewData = from person in data.people
                           join place in data.places on person.place_id equals place.id
                           select new PersonViewModel { id = person.id, name = person.name, gender = person.gender, father_id = person.father_id, mother_id = person.mother_id, place_id = person.place_id, level = person.level, BirthPlace = place.name };

            return viewData.Select(x=> PersonToLuceneDocumentConverter.Convert(x, AncestorsDictionary[x.id],DescendantsDictionary[x.id]));
        }

        private static string IndexRoot
        {
            get
            {
                var path = Path.Combine(ConfigurationManager.AppSettings["IndexRoot"], string.Format("idx_{0:yyyyMMddHHmmss}", DateTime.UtcNow));
                Directory.CreateDirectory(path);
                return path;
            }
        }




        private static string WriteToIndex(IEnumerable<Document> documents)
        {
            using (var indexWriter = new IndexWriter(FSDirectory.Open(new DirectoryInfo(IndexRoot)), new Lucene.Net.Analysis.SimpleAnalyzer(), IndexWriter.MaxFieldLength.LIMITED))
            {
                foreach (var document in documents)
                {
                    indexWriter.AddDocument(document);
                }
                indexWriter.Optimize();
                indexWriter.Commit();
            }
            return string.Empty;
        }

        private static Dictionary<int, string> AncestorsDictionary;

        private static void BuildAncestors(JsonData data)
        {
            var people = data.people;
            foreach (var person in people)
            {
                var personid = person.id;
                var idList = new List<int>();
                GetAncestors(person, idList);
                AncestorsDictionary.Add(personid, string.Join(",",idList));
            }
        }

        private static void GetAncestors(Person person, List<int> idList)
        {
            var fatherId = person.father_id.HasValue ? person.father_id.Value : 0;
            var motherId = person.mother_id.HasValue ? person.mother_id.Value : 0;
            if (fatherId > 0)
            {
                idList.Add(fatherId);
                var father = jsonData.people.FirstOrDefault(x => x.id == fatherId);
                if (father != null)
                {
                    GetAncestors(father, idList);
                }
            }

            if (motherId > 0)
            {
                idList.Add(motherId);
                var mother = jsonData.people.FirstOrDefault(x => x.id == motherId);
                if (mother != null)
                {
                    GetAncestors(mother, idList);
                }
            }
        }
        private static void BuildDescendants(JsonData data)
        {
            var people = data.people;
            foreach (var person in people)
            {
                var personid = person.id;
                var idList = new List<int>();
                GetDescendants(person, idList);
                DescendantsDictionary.Add(personid, string.Join(",", idList));
            }
        }

        private static void GetDescendants(Person person, List<int> idList)
        {
            var fatherList = jsonData.people.Where(x => x.father_id.GetValueOrDefault() == person.id);
            if (fatherList.Any())
            {
                foreach (var father in fatherList)
                {
                    idList.Add(father.id);
                    GetDescendants(father, idList);
                }
            }

            var motherList = jsonData.people.Where(x => x.mother_id.GetValueOrDefault() == person.id);
            if (motherList.Any())
            {
                foreach (var mother in motherList)
                {
                    idList.Add(mother.id);
                    GetDescendants(mother, idList);
                }
            }
        }


        private static Dictionary<int, string> DescendantsDictionary;

        private static JsonData jsonData;

        static void Main(string[] args)
        {
            jsonData = GetJsonDataObject();
            AncestorsDictionary = new Dictionary<int, string>();
            BuildAncestors(jsonData);
            DescendantsDictionary = new Dictionary<int, string>();
            BuildDescendants(jsonData);
            var documents = GetDocuments(jsonData);
            var result = WriteToIndex(documents);


        }
    }
}
