﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Newtonsoft.Json;

namespace Services.JsonFile
{
    public class Loader : ILoader
    {
        private readonly string _fileName;
        public Loader(string fileName)
        {
            _fileName = fileName;
        }
        public JsonData Load()
        {
            string json = File.ReadAllText(_fileName);
            return JsonConvert.DeserializeObject<JsonData>(json);
        }
    }
}
