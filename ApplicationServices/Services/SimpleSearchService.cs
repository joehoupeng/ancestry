﻿using System.Collections.Generic;
using System.Linq;
using Data;

namespace Services
{
    public interface ISimpleSearchService
    {
        IEnumerable<PersonViewModel> Search(string keyword = "", bool isMale = false, bool isFemale = false);
    }
    public class SimpleSearchService : ISimpleSearchService
    {
        public IEnumerable<PersonViewModel> Search(string keyword = "", bool isMale = false, bool isFemale = false)
        {
            var list = new List<PersonViewModel>();
            list.Add(new PersonViewModel());
            return list;
        }
    }
}
