﻿using System.IO;
using Data;
using Newtonsoft.Json;

namespace AppServices.JsonFile
{
    public class Loader : ILoader
    {
        private readonly string _fileName;
        public Loader(string fileName)
        {
            _fileName = fileName;
        }
        public JsonData Load()
        {
            string json = File.ReadAllText(_fileName);
            return JsonConvert.DeserializeObject<JsonData>(json);
        }
    }
}
