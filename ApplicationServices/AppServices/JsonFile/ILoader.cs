﻿using Data;

namespace AppServices.JsonFile
{
    public interface ILoader
    {
        JsonData Load();
    }
}
